var mongoose = require('mongoose');

const PromotionBannerSchema = new mongoose.Schema({
    tagTitle: String,
    tagDescription: String,
    imageName: String,
    addedDate: Date,
    link: String,
    position: Number,
    modifiedDate: Date,
    status: String,
    imageLink: String
});
const promotion = mongoose.model('promotionbanner', PromotionBannerSchema);
module.exports = promotion;