var mongoose = require('mongoose');

const CommunitySchema = new mongoose.Schema({
    name: String,
    email: String,
    profileImageName: String
    
});
const Community = mongoose.model('community-user', CommunitySchema);
module.exports = Community;