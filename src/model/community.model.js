var mongoose = require('mongoose');

const CommunitySchema = new mongoose.Schema({
    heading: String,
    subHeading: String,
    bannerImage: [{
        bannerImageName: String
    }],
    communityDescription: [{
        description: String
    }]
});
const Community = mongoose.model('community', CommunitySchema);
module.exports = Community;