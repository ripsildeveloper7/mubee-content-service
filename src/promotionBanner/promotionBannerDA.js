var PromotionBanner = require('../model/promotionBanner.model');
var env = require('../config/s3.env');

exports.createPromotionBanner = function(req, res) {
    var create = new PromotionBanner(req.body);
    create.status = 'Enabled';
    create.addedDate = Date.now();
    create.save(function(err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}
exports.updatePromotionBannerName = function(req, res) {
    PromotionBanner.findOne({'_id': req.params.id}).select().exec(function(err, promotion) {
        if (err) {
            res.status(500).json(err);
        } else {
            promotion.imageName = req.body.imageName;
            promotion.modifiedDate = Date.now();
            promotion.save(function(err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(updateData);
                }
            })
        }
    })
}
exports.getPromotionBanner = function(req, res) {
    PromotionBanner.find({}).select().exec(function(err, promotion) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (promotion.length !== 0) {
                for (const data of promotion) {
                    data.imageName = env.ImageServerPath + 'promotionbanner' + '/' + req.params.id + '/' + data.imageName;
                }
            }
            res.status(200).json(promotion);
        }
    })
}
exports.getSinglePromotionBanner = function(req, res) {
    PromotionBanner.findOne({'_id': req.params.id}).select().exec(function(err, promotion) {
        if (err) {
            res.status(500).json(err);
        } else {
            promotion.imageLink = env.ImageServerPath + 'promotionbanner' + '/' + req.params.id + '/' + promotion.imageName;
            res.status(200).json(promotion);
        }
    })
}
exports.updatePromotionBannerStatus = function(req, res) {
    PromotionBanner.findOne({'_id': req.params.id}).select().exec(function(err, promotion) {
        if (err) {
            res.status(500).json(err);
        } else {
            promotion.status = req.body.status;
            promotion.modifiedDate = Date.now();
            promotion.save(function(err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    PromotionBanner.findOne({position: req.body.position}).select().exec(function(err, promotion) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                           /*  if (promotion.length !== 0) {
                                for (const data of promotion) {
                                    data.imageName = env.ImageServerPath + 'promotionbanner' + '/' + data._id + '/' + data.imageName;
                                }
                            } */
                            promotion.imageName = env.ImageServerPath + 'promotionbanner' + '/' + req.params.id + '/' + promotion.imageName;
                            res.status(200).json(promotion);
                        }
                    })
                }
            })
        }
    })
}
exports.deletePromotionBanner = function(req, res) {
    PromotionBanner.findOneAndRemove({'_id': req.params.id}).select().exec(function(err, deleteBanner) {
        if (err) {
            res.status(500).json(err);
        } else {
            PromotionBanner.find({}).select().exec(function(err, promotion) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    if (promotion.length !== 0) {
                        for (const data of promotion) {
                            data.imageName = env.ImageServerPath + 'promotionbanner' + '/' + req.params.id + '/' + data.imageName;
                        }
                    }
                    res.status(200).json(promotion);
                }
            })
        }
    })
}
exports.getPromotionBannerForUI = function(req, res) {
    PromotionBanner.findOne({status: 'Enabled', position: req.body.position}).select().exec(function(err, promotion) {
        if (err) {
            res.status(500).json(err);
        } else {
           /*  if (promotion.length !== 0) {
                for (const data of promotion) {
                    data.imageName = env.ImageServerPath + 'promotionbanner' + '/' + req.params.id + '/' + data.imageName;
                }
            } */
            promotion.imageName = env.ImageServerPath + 'promotionbanner' + '/' + promotion.id + '/' + promotion.imageName;
            res.status(200).json(promotion);
        }
    })
}
exports.getSinglePromotionBannerForAdmin = function(req, res) {
    PromotionBanner.find({position: req.body.position}).select().exec(function(err, promotion) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (promotion.length !== 0) {
                for (const data of promotion) {
                    data.imageName = env.ImageServerPath + 'promotionbanner' + '/' + data._id + '/' + data.imageName;
                }
            }
            res.status(200).json(promotion);
        }
    })
}
exports.updatePromotionBanner = function(req, res) {
    PromotionBanner.findOne({'_id': req.params.id}).select().exec(function(err, promotion) {
        if (err) {
            res.status(500).json(err);
        } else {
            promotion.tagTitle = req.body.tagTitle;
            promotion.tagDescription = req.body.tagDescription;
            promotion.link = req.body.link;
            promotion.modifiedDate = Date.now();
            promotion.imageName = req.body.imageName;
            promotion.save(function(err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(updateData);
                }
            })
        }
    })
}