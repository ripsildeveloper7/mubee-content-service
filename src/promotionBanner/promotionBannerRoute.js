var PromotionBannerMgr = require('./promotionBannerMgr');

module.exports = function (app) {
    app.route('/createpromotionbanner').post(PromotionBannerMgr.createPromotionBanner); 
    app.route('/updatepromotionbannername/:id').put(PromotionBannerMgr.updatePromotionBannerName); 
    app.route('/getpromotionbanner').get(PromotionBannerMgr.getPromotionBanner); 
    app.route('/getsinglepromotionbanner/:id').get(PromotionBannerMgr.getSinglePromotionBanner); 
    app.route('/updatepromotionbannerstatus/:id').put(PromotionBannerMgr.updatePromotionBannerStatus); 
    app.route('/deletepromotionbanner/:id').delete(PromotionBannerMgr.deletePromotionBanner); 
    app.route('/getpromotionbannerforui').post(PromotionBannerMgr.getPromotionBannerForUI); 
    app.route('/getpromotionbannerforAdmin').post(PromotionBannerMgr.getSinglePromotionBannerForAdmin); 
    app.route('/updatepromotionbanner/:id').put(PromotionBannerMgr.updatePromotionBanner);
}