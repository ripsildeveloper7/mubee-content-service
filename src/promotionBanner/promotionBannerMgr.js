var PromotionBannerDA = require('./promotionBannerDA');

exports.createPromotionBanner = function (req, res) {
    try {
        PromotionBannerDA.createPromotionBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.updatePromotionBannerName = function (req, res) {
    try {
        PromotionBannerDA.updatePromotionBannerName(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getPromotionBanner = function (req, res) {
    try {
        PromotionBannerDA.getPromotionBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getSinglePromotionBanner = function (req, res) {
    try {
        PromotionBannerDA.getSinglePromotionBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.updatePromotionBannerStatus = function (req, res) {
    try {
        PromotionBannerDA.updatePromotionBannerStatus(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.deletePromotionBanner = function (req, res) {
    try {
        PromotionBannerDA.deletePromotionBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getPromotionBannerForUI = function (req, res) {
    try {
        PromotionBannerDA.getPromotionBannerForUI(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getSinglePromotionBannerForAdmin = function (req, res) {
    try {
        PromotionBannerDA.getSinglePromotionBannerForAdmin(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.updatePromotionBanner = function (req, res) {
    try {
        PromotionBannerDA.updatePromotionBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}