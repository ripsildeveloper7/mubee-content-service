var aboutDA = require('./aboutusDA');



exports.createAboutUs = function (req, res) {
    try {
        aboutDA.createAboutUs(req, res);

    } catch (error) {
        console.log(error);
    }
}


exports.getAboutUs = function (req, res) {
    try {
        aboutDA.getAboutUs(req, res);

    } catch (error) {
        console.log(error);
    }
}

exports.deleteAboutUs = function (req, res) {
    try {
        aboutDA.deleteAboutUs(req, res);

    } catch (error) {
        console.log(error);
    }
}

exports.updateAboutUs = function (req, res) {
    try {
        aboutDA.updateAboutUs(req, res);

    } catch (error) {
        console.log(error);
    }
}

exports.getSingleAboutUs = function (req, res) {
    try {
        aboutDA.getSingleAboutUs(req, res);

    } catch (error) {
        console.log(error);
    }
}
