var aboutUs = require('../model/aboutUs.model');



exports.createAboutUs = function (req, res) {
    var about = new aboutUs();
    about.Title = req.body.Title;
    about.Details = req.body.Details;
    about.note1 = req.body.note1;
    about.note2 = req.body.note2;
    about.note3 = req.body.note3;
    about.save(function (err, data) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while saving data'
            })
        } else {
            res.status(200).json(data);
            // aboutUs.find({}).select().exec(function (err, about) {
            //     if (err) {
            //         res.status(500).send({
            //             "result": 'error occured while retreiving data'
            //         })
            //     } else {
            //         res.status(200).json(about);
            //     }
            // })
        }
    })
}

exports.getAboutUs = function (req, res) {
    aboutUs.find({}).select().exec(function (err, termsData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            res.status(200).json(termsData);
        }
    })
}

exports.deleteAboutUs = function (req, res) {
    aboutUs.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.status(500).send({
                "result": 0
            });
        } else {

            aboutUs.find({}).select().exec(function (err, about) {
                if (err) {
                    res.status(500).send({
                        message: "Some error occurred while retrieving notes."
                    });
                } else {

                    res.status(200).json(about);
                }
            });


        }
    });
}


exports.updateAboutUs = function (req, res) {
    aboutUs.find({
        '_id': req.params.id,
    }, function (err, about) {
        if (err) {
            res.status(500).send({
                message: "Some error occurred while retrieving notes."
            });
        } else {
            about[0].Title = req.body.Title;
            about[0].Details = req.body.Details;
            about[0].note1 = req.body.note1;
            about[0].note2 = req.body.note2;
            about[0].note3 = req.body.note3;
            about[0].ImageName = req.body.ImageName;
            about[0].save(function (err, result) {
                if (err) {
                    console.log(err);
                    res.status(500).send({
                        message: 1
                    });
                } else {
                    aboutUs.find({}).select().exec(function (err, about) {
                        if (err) {
                            res.status(500).send({
                                "result": 'error occured while retreiving data'
                            })
                        } else {
                            res.status(200).json(about);
                        }
                    })
                }
            })
        }
    });
}

exports.getSingleAboutUs = function (req, res) {
    aboutUs.findOne({'_id': req.params.id}).select().exec(function (err, about) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            res.status(200).json(about);
        }
    })
}