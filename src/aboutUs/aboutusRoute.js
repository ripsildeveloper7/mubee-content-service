var aboutMgr = require('./aboutusMgr');

module.exports = function (app) {
    app.route('/aboutus')
        .post(aboutMgr.createAboutUs); // Create Term and Use

    app.route('/getaboutus')
        .get(aboutMgr.getAboutUs); // Get Terms and Use

    app.route('/deleteaboutus/:id')
        .delete(aboutMgr.deleteAboutUs); // Delete Terms and Use

    app.route('/editaboutus/:id')
        .put(aboutMgr.updateAboutUs); // Update Terms and Use

    app.route('/getaboutsingle/:id')
        .get(aboutMgr.getSingleAboutUs); // Update Terms and Use
}