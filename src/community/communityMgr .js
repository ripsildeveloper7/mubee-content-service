var communityDA = require('./communityDA');

exports.createCommunityPage = function(req, res) {
    try {
        communityDA.createCommunityPage(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getAllCommunityDetail = function(req, res) {
    try {
        communityDA.getAllCommunityDetail(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.editCommunityPageHeader = function(req, res) {
    try {
        communityDA.editCommunityPageHeader(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.editCommunityPageSubHeader = function(req, res) {
    try {
        communityDA.editCommunityPageSubHeader(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.editCommunityPageDescription = function(req, res) {
    try {
        communityDA.editCommunityPageDescription(req, res);
    } catch (error) {
        console.log(error);
    }
}


exports.createCommunityRegister = function(req, res) {
    try {
        communityDA.createCommunityRegister(req, res);
    } catch (error) {
        console.log(error);
    }
}
exports.getAllCommunityUser = function(req, res) {
    try {
        communityDA.getAllCommunityUser(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadSingleUserImage = function(req, res) {
    try {
        communityDA.uploadSingleUserImage(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.uploadSingleBanner = function(req, res) {
    try {
        communityDA.uploadSingleBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.editSingleBanner = function(req, res) {
    try {
        communityDA.editSingleBanner(req, res);
    } catch (error) {
        console.log(error);
    }
}
