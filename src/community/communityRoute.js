var communityMgr = require('./communityMgr ');

module.exports = function(app) {
    app.route('/createcommunitypage')
    .post(communityMgr.createCommunityPage);  
    app.route('/editcommunitypageheader/:communityId')
    .put(communityMgr.editCommunityPageHeader);
    app.route('/editcommunitypagesubheader/:communityId')
    .put(communityMgr.editCommunityPageSubHeader);
    app.route('/editcommunitypagedescription/:communityId/:descriptionId')
    .put(communityMgr.editCommunityPageDescription);  
    app.route('/getallcommunity')
    .get(communityMgr.getAllCommunityDetail); 
    app.route('/createbannerimage/:communityId')
    .put(communityMgr.uploadSingleBanner);
    app.route('/createbannerimage/:communityId/:bannerId/:imageName')
    .put(communityMgr.editSingleBanner);
    
    app.route('/createcommunityregister')
    .post(communityMgr.createCommunityRegister);  
    app.route('/getallcommunityuser')
    .get(communityMgr.getAllCommunityUser); 
    app.route('/createuserimage/:userId')
    .put(communityMgr.uploadSingleUserImage);
}