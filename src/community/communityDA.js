var Community = require('../model/community.model');
var CommunityUser = require('../model/community-user.model');
var s3 = require('../config/s3.config');
var env = require('../config/s3.env');

exports.createCommunityPage = function (req, res) {
    Community.find({}).select().exec(function (err, allCommuityData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
           if(allCommuityData.length === 0) {
            var community = new Community();
            community.heading = req.body.heading;
            community.subHeading = req.body.subHeading;
            community.communityDescription = req.body.communityDescription;
            community.save(function(err, communityData) {
                if(err) {
                    res.status(500).send({
                        "result": "error occured while saving data"
                    });
                } else {
                    res.status(200).json(communityData);
                }
            });
           } else {
            res.status(200).send({
                "result": 'alredy community data inserted' 
            })
           }
        }
    });
}


exports.editCommunityPageHeader = function (req, res) {
    Community.findOneAndUpdate({
        _id: req.params.communityId
    }, {heading: req.body.heading }, {
        new: true
      }).select().exec(function (err, updateData) {
        if (err) {
            res.status(500).send({
                message: "Some error occurred while retrieving notes."
            });
        } else {
            res.status(200).json(updateData);
        }
    });
}

exports.editCommunityPageSubHeader = function (req, res) {
    Community.findOneAndUpdate({
        _id: req.params.communityId
    }, { subHeading: req.body.subHeading }, {
        new: true
      }).select().exec(function (err, updateData) {
        if (err) {
            res.status(500).send({
                message: "Some error occurred while retrieving notes."
            });
        } else {
            res.status(200).json(updateData);
        }
    });
}

exports.editCommunityPageDescription = function (req, res) {
    Community.findOneAndUpdate({
        _id: req.params.communityId,
        "communityDescription._id": req.params.descriptionId
    }, { $set: { "communityDescription.$.description" : req.body.description } }, {
        new: true
      }).select().exec(function (err, updateData) {
        if (err) {
            res.status(500).send({
                message: "Some error occurred while retrieving notes."
            });
        } else {
            res.status(200).json(updateData);
        }
    });
}
exports.getAllCommunityDetail = function (req, res) {
    Community.findOne({}).select().exec(function (err, allCommuityData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            res.status(200).json(allCommuityData);
        }
    });
}

exports.uploadSingleBanner = function (req, res) {
    var params = {
        Bucket: env.Bucket, 
        /* Marker:   + '/' + req.params.communityId, */
        Prefix: 'images' + '/' + 'community' + '/' + req.params.communityId
       };
       s3.listObjects(params, function(err, data) {
         if (err) console.log(err, err.stack); // an error occurred
         else    {
            const base64Data = Buffer.from(req.body.fileName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
            const type = req.body.fileName.split(';')[0].split('/')[1];
            const params = {
                Bucket: env.Bucket + '/' + 'images' + '/' + 'community' + '/' + req.params.communityId, // create a folder and save the image
                Key: req.body.bannerImageName,
                ACL: 'public-read',
                ContentEncoding: 'base64',
                Body: base64Data,   
                ContentType: `image/${type}`
            };
            s3.upload(params, function (err, data) {
                if (err) {
                    console.log(err);
                } else {
                    Community.findOneAndUpdate({
                        _id: req.params.communityId
                    }, { $push: { bannerImage: { bannerImageName: req.body.bannerImageName } }}, 
                    {new: true
                  }).select().exec(function (err, updateData) {
                        if (err) {
                            res.status(500).send({
                                message: "Some error occurred while retrieving notes."
                            });
                        } else {
                            res.status(200).json(updateData);
                        }
                    });
                }
                });
        }
    });
}
        exports.editSingleBanner = function (req, res) {
            var params = {
                Bucket: env.Bucket, 
                /* Marker:   + '/' + req.params.communityId, */
                Prefix: 'images' + '/' + 'community' + '/' + req.params.communityId + '/' + req.params.imageName
               };
               s3.listObjects(params, function(err, data) {
                 if (err) console.log(err, err.stack); // an error occurred
                 else    {
                    if (data.Contents.length === 0) {
                        const base64Data = Buffer.from(req.body.fileName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                        const type = req.body.fileName.split(';')[0].split('/')[1];
                        const params = {
                            Bucket: env.Bucket + '/' + 'images' + '/' + 'community' + '/' + req.params.communityId, // create a folder and save the image
                            Key: req.body.bannerImageName,
                            ACL: 'public-read',
                            ContentEncoding: 'base64',
                            Body: base64Data,
                            ContentType: `image/${type}`
                        };
                        s3.upload(params, function (err, data) {
                            if (err) {
                                console.log(err);
                            } else {
                                Community.findOneAndUpdate({
                                    _id: req.params.communityId,
                                    "bannerImage._id": req.params.bannerId,
                                }, { $set: { "bannerImage.$.bannerImageName" : req.body.bannerImageName } }, 
                                {new: true
                              }).select().exec(function (err, updateData) {
                                    if (err) {
                                        res.status(500).send({
                                            message: "Some error occurred while retrieving notes."
                                        });
                                    } else {
                                        res.status(200).json(updateData);
                                    }
                                });
                            }
                            });
        
                 } else {
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'community' + '/' + req.params.communityId + '/' + req.params.imageName
                    }, function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            const base64Data =  Buffer.from(req.body.fileName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                            const type = req.body.fileName.split(';')[0].split('/')[1];
                            const params = {
                                Bucket: env.Bucket + '/' + 'images' + '/' + 'community' + '/' + req.params.communityId, // create a folder and save the image
                                Key: req.body.bannerImageName,
                                ACL: 'public-read',
                                ContentEncoding: 'base64',
                                Body: base64Data,
                                ContentType: `image/${type}`
                            };
                            s3.upload(params, function (err, data) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    Community.findOneAndUpdate({
                                        _id: req.params.communityId,
                                        "bannerImage._id": req.params.bannerId,
                                    }, { $set: { "bannerImage.$.bannerImageName" : req.body.bannerImageName } }, {
                                        new: true
                                      }).select().exec(function (err, updateData) {
                                        if (err) {
                                            res.status(500).send({
                                                message: "Some error occurred while retrieving notes."
                                            });
                                        } else {
                                            res.status(200).json(updateData);
                                        }
                                    });
                                }
                                });
                            } 
                        });
                    }      
                    } })
                }
        

exports.createCommunityRegister = function (req, res) {
    CommunityUser.findOne({email: req.body.email}).select().exec(function (err, allCommuityData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
           if(!allCommuityData) {
            var community = new CommunityUser();
            community.name = req.body.name;
            community.email = req.body.email;
            community.save(function(err, communityData) {
                if(err) {
                    res.status(500).send({
                        "result": "error occured while saving data"
                    });
                } else {
                    res.status(200).json(communityData);
                 /*    CommunityUser.find({}).select().exec(function (err, allCommuityData) {
                        if (err) {
                            res.status(500).send({
                                "result": 'error occured while retreiving data'
                            })
                        } else {
                            res.status(200).json(allCommuityData);
                        }
                    }); */
                }
            });
           } else {
            res.status(200).send({
                "result": 'alredy email inserted' 
            })
           }
        }
    });
}

exports.getAllCommunityUser = function (req, res) {
    CommunityUser.find({}).select().exec(function (err, allCommuityData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            res.status(200).json(allCommuityData);
        }
    });
}

exports.getAllCommunityUser = function (req, res) {
    CommunityUser.find({}).select().exec(function (err, allCommuityData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            res.status(200).json(allCommuityData);
        }
    });
}


exports.uploadSingleUserImage = function (req, res) {
    var params = {
        Bucket: env.Bucket, 
        /* Marker:   + '/' + req.params.communityId, */
        Prefix: 'images' + '/' + 'community-user' + '/' + req.params.userId
       };
       s3.listObjects(params, function(err, data) {
         if (err) console.log(err, err.stack); // an error occurred
         else    {
            if (data.Contents.length === 0) {
                const base64Data = Buffer.from(req.body.fileName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                const type = req.body.fileName.split(';')[0].split('/')[1];
                const params = {
                    Bucket: env.Bucket + '/' + 'images' + '/' +  'community-user' + '/' + req.params.userId, // create a folder and save the image
                    Key: req.body.profileImageName,
                    ACL: 'public-read',
                    ContentEncoding: 'base64',
                    Body: base64Data,
                    ContentType: `image/${type}`
                };
                s3.upload(params, function (err, data) {
                    if (err) {
                        console.log(err);
                    } else {
                        CommunityUser.findOneAndUpdate({
                            _id: req.params.userId
                        }, { profileImageName: req.body.profileImageName  }, 
                        {new: true
                      }).select().exec(function (err, updateData) {
                            if (err) {
                                res.status(500).send({
                                    message: "Some error occurred while retrieving notes."
                                });
                            } else {
                                /* res.status(200).json(updateData); */
                                CommunityUser.find({}).select().exec(function (err, allCommuityData) {
                                    if (err) {
                                        res.status(500).send({
                                            "result": 'error occured while retreiving data'
                                        })
                                    } else {
                                        res.status(200).json(allCommuityData);
                                    }
                                });
                            }
                        });
                    }
                    });

         } else {
            s3.deleteObject({
                Bucket: env.Bucket,
                Key: data.Contents[0].Key
            }, function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    const base64Data =  Buffer.from(req.body.fileName.replace(/^data:image\/\w+;base64,/, ""), 'base64');
                    const type = req.body.fileName.split(';')[0].split('/')[1];
                    const params = {
                        Bucket: env.Bucket + '/' + 'images' + '/' + 'community-user' + '/' + req.params.userId, // create a folder and save the image
                        Key: req.body.profileImageName,
                        ACL: 'public-read',
                        ContentEncoding: 'base64',
                        Body: base64Data,
                        ContentType: `image/${type}`
                    };
                    s3.upload(params, function (err, data) {
                        if (err) {
                            console.log(err);
                        } else {
                            CommunityUser.findOneAndUpdate({
                                _id: req.params.userId
                            }, { "profileImageName": req.body.profileImageName}, {
                                new: true
                              }).select().exec(function (err, updateData) {
                                if (err) {
                                    res.status(500).send({
                                        message: "Some error occurred while retrieving notes."
                                    });
                                } else {
                                    /* res.status(200).json(updateData); */
                                    CommunityUser.find({}).select().exec(function (err, allCommuityData) {
                                        if (err) {
                                            res.status(500).send({
                                                "result": 'error occured while retreiving data'
                                            })
                                        } else {
                                            res.status(200).json(allCommuityData);
                                        }
                                    });
                                }
                            });
                        }
                        });
                    } 
                });
            }      
            } })
        }